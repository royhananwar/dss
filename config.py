"""
Config File Class
"""
import os

from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)


class Config(object):
    """
    Base Config
    """

    SQLALCHEMY_TRACK_MODIFICATIONS = True
    
    POST_PER_PAGE = 3


class DevelopmentConfig(Config):
    """
    Development Class
    """
    DEBUG = True

    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    SECRET_KEY = os.environ.get('SECRET_KEY')
    PATH_UPLOAD = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'app', 'static', 'uploaded_dev')
    
    ELASTICSEARCH_HOST = 'http://localhost:9200'


class ProductionConfig(Config):
    """
    Production Config
    """

    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    SECRET_KEY = os.environ.get('SECRET_KEY')
    PATH_UPLOAD = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'app', 'static', 'upload_prod')

    ELASTICSEARCH_HOST = 'http://localhost:9200'
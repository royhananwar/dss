
import os
import logging
from dotenv import load_dotenv

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

import config
from app.utils.logging_util import default_logger

# load .env file
# need to load first before create_app
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), ".env")
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()


def register_extensions(app):
    """
    Register Extension
    
    Arguments:
        app {flask object} -- target app
    """
    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)


def register_blueprints(app):
    """
    Register Blueprints
    
    Arguments:
        app {flask object} -- target app
    """

    from app.document import document_blueprint
    from app.authentication import authentication_blueprint

    app.register_blueprint(document_blueprint)
    app.register_blueprint(authentication_blueprint)


def create_app(config_name=config.DevelopmentConfig):
    """
    Initiate APP
    
    Keyword Arguments:
        config_name {config object} -- (default: {config.DevelopmentConfig})
    """

    app = Flask(__name__)
    app.config.from_object(config_name)

    # set default_logging here
    logger = default_logger(__name__)
    logger.info('Application Run')

    register_extensions(app)
    register_blueprints(app)

    return app
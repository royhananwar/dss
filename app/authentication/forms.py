from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectMultipleField, SelectField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    username = StringField(
        'Username', 
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
            'placeholder': 'Username'
        }
    )
    password = PasswordField(
        'Password', 
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
            'placeholder': 'password'
        }
    )
    remember_me = BooleanField(
        'Remember Me',
        render_kw={
            'class': 'custom-control-input',
        }
    )
    submit = SubmitField(
        'Sign In',
        render_kw={
            'class': 'btn btn-lg btn-primary btn-block text-uppercase'
        }
    )


class PermissionUserTagForm(FlaskForm):
    username = SelectField(
        'Username', 
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        },
        coerce=int
    )
    tags = SelectMultipleField(
        'Tags',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control select2-tags',
        },
        coerce=int
    )

class PermissionUserTagEditForm(FlaskForm):
    username = StringField(
        'Username', 
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
            'readonly': 'readonly'
        }
    )
    tags = SelectMultipleField(
        'Tags',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control select2-tags',
        },
        coerce=int
    )


class PermissionUserDocumentForm(FlaskForm):
    username = SelectField(
        'Username', 
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        },
        coerce=int
    )
    documents = SelectMultipleField(
        'Document',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control select2-documents',
        },
        coerce=int
    )

class PermissionUserDocumentEditForm(FlaskForm):
    username = StringField(
        'Username', 
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
            'readonly': 'readonly'
        }
    )
    documents = SelectMultipleField(
        'tag',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control select2-documents',
        },
        coerce=int
    )


class UserForm(FlaskForm):
    username = StringField(
        'Username',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        }
    )
    email = StringField(
        'Email',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        }
    )
    full_name = StringField(
        'Full Name',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        }
    )
    role = SelectField(
        'Role',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control select2-role'
        },
        choices=[(10, 'Admin'), (1, 'User')],
        coerce=int
    )
    password = PasswordField(
        'Password',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control'
        },
    )


class UserEditForm(FlaskForm):
    username = StringField(
        'Username',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        }
    )
    email = StringField(
        'Email',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        }
    )
    full_name = StringField(
        'Full Name',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control',
        }
    )
    role = SelectField(
        'Role',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control select2-role'
        },
        choices=[(10, 'Admin'), (1, 'User')],
        coerce=int
    )


class ChangePasswordForm(FlaskForm):
    old_password = PasswordField(
        'Old Password',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control'
        },
    )
    new_password = PasswordField(
        'New Password',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control'
        },
    )
    confirmed_password = PasswordField(
        'Confirmed New Password',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control'
        },
    )
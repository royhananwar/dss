from flask import abort
from flask import redirect, render_template, url_for, flash, current_app, request
from flask_login import current_user, login_user, logout_user, login_required

from app.authentication import authentication_blueprint as bp
from app.models.authentication import User, UserPermissionTag, UserPermissionDocument
from app.models.document import Tag, Document
from app.authentication.forms import (
    LoginForm, PermissionUserTagForm, PermissionUserTagEditForm, 
    PermissionUserDocumentForm, PermissionUserDocumentEditForm,
    UserForm, UserEditForm, ChangePasswordForm
)
from app.utils.logging_util import default_logger
from app import db

@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('document_blueprint.index_document'))
    form = LoginForm()
    if form.validate_on_submit():
        user = db.session.query(User).filter(User.username==form.username.data).first()
        if not user or not user.check_password(form.password.data):
            print('Invalid username or password')
            flash('Invalid username or password')
            return redirect(url_for('authentication_blueprint.login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('document_blueprint.index_document'))
    return render_template('authentication/login.html', title='Sign In', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('authentication_blueprint.login'))


@bp.route('/permission/user/tag/index')
@login_required
def index_permission_user_tag():
    logger = default_logger(__name__)
    users = db.session.query(
        User
    ).outerjoin(
        UserPermissionTag, UserPermissionTag.id_user == User.id
    ).filter(
        User.role != 0
    ).all()
    
    return render_template('/permission/tag/index.html', users=users)


@bp.route('/permission/user/tag/create', methods=['GET', 'POST'])
@login_required
def create_permission_user_tag():
    logger = default_logger(__name__)
    form = PermissionUserTagForm()
    user_have_permission = db.session.query(UserPermissionTag.id_user).all()
    list_user_have_permission = [id_user for id_user in user_have_permission]
    user_query = db.session.query(User).filter(
        ~User.id.in_(list_user_have_permission),
        User.role != 0
    ).all()
    tag_query = db.session.query(Tag).all()
    user_choices = [(int(user.id), user.username) for user in user_query] 
    tag_choices = [(int(tag.id), tag.name) for tag in tag_query]

    form.tags.choices = tag_choices
    form.username.choices = user_choices

    if form.validate_on_submit():
        id_user = form.username.data
        tags = form.tags.data

        for tag in tags:
            user_permission_tag = UserPermissionTag(
                id_user=id_user,
                id_tag=tag
            )
            db.session.add(user_permission_tag)
        db.session.commit()
        return redirect(url_for('authentication_blueprint.index_permission_user_tag'))
    return render_template('permission/tag/create.html', form=form)

@bp.route('/permission/user/tag/edit/<int:id_user>', methods=['GET', 'POST'])
@login_required
def edit_permission_user_tag(id_user):
    logger = default_logger(__name__)
    user_permission_tag = db.session.query(
        UserPermissionTag
    ).join(
        User, User.id == UserPermissionTag.id_user
    ).join(
        Tag, Tag.id == UserPermissionTag.id_tag
    ).filter(
        UserPermissionTag.id_user == id_user
    ).all()
    user_detail = db.session.query(User).filter(User.id == id_user).first()
    form = PermissionUserTagEditForm(obj=user_permission_tag)
    tag_query = db.session.query(Tag).all()
    tag_choices = [(int(tag.id), tag.name) for tag in tag_query]

    form.username.data = user_detail.username
    form.tags.choices = tag_choices
    if form.validate_on_submit():
        id_user = form.username.data
        tags = form.tags.data

        # delete the old user permission tag
        old_user_permission_tags = db.session.query(UserPermissionTag).filter(id_user == id_user).all()
        for old_data in old_user_permission_tags:
            db.session.delete(old_data)
        
        for tag in tags:
            user_permission_tag = UserPermissionTag(
                id_user=id_user,
                id_tag=tag
            )
            db.session.add(user_permission_tag)
        db.session.commit()
        return redirect(url_for('authentication_blueprint.index_permission_user_tag'))
    return render_template('permission/tag/edit.html', form=form)

@bp.route('/permission/user/tag/delete/<int:id_user>', methods=['POST'])
@login_required
def delete_permission_user_tag(id_user):
    logger = default_logger(__name__)
    user_permission_tags = db.session.query(UserPermissionTag).filter(UserPermissionTag.id_user == id_user).all()
    for user_permission_tag in user_permission_tags:
        db.session.delete(user_permission_tag)
    db.session.commit()
    return redirect(url_for('authentication_blueprint.index_permission_user_tag'))


@bp.route('/permission/user/document/index')
@login_required
def index_permission_user_document():
    logger = default_logger(__name__)
    users = db.session.query(
        User
    ).outerjoin(
        UserPermissionDocument, UserPermissionDocument.id_user == User.id
    ).filter(
        User.role != 0
    ).all()
    
    return render_template('/permission/document/index.html', users=users)


@bp.route('/permission/user/document/create', methods=['GET', 'POST'])
@login_required
def create_permission_user_document():
    logger = default_logger(__name__)
    form = PermissionUserDocumentForm()
    user_have_permission = db.session.query(UserPermissionDocument.id_user).all()
    list_user_have_permission = [id_user for id_user in user_have_permission]
    user_query = db.session.query(User).filter(
        ~User.id.in_(list_user_have_permission),
        User.role != 0
    ).all()
    document_query = db.session.query(Document).all()
    user_choices = [(int(user.id), user.username) for user in user_query] 
    document_choices = [(int(document.id), document.title) for document in document_query]

    form.documents.choices = document_choices
    form.username.choices = user_choices

    if form.validate_on_submit():
        id_user = form.username.data
        documents = form.documents.data

        for document in documents:
            user_permission_document = UserPermissionDocument(
                id_user=id_user,
                id_document=document
            )
            db.session.add(user_permission_document)
        db.session.commit()
        return redirect(url_for('authentication_blueprint.index_permission_user_document'))
    return render_template('permission/document/create.html', form=form)


@bp.route('/permission/user/document/edit/<int:id_user>', methods=['GET', 'POST'])
@login_required
def edit_permission_user_document(id_user):
    logger = default_logger(__name__)
    user_permission_document = db.session.query(
        UserPermissionDocument
    ).join(
        User, User.id == UserPermissionDocument.id_user
    ).join(
        Document, Document.id == UserPermissionDocument.id_document
    ).filter(
        UserPermissionDocument.id_user == id_user
    ).all()
    user_detail = db.session.query(User).filter(User.id == id_user).first()
    form = PermissionUserDocumentEditForm(obj=user_permission_document)
    document_query = db.session.query(Document).all()
    document_choices = [(int(document.id), document.title) for document in document_query]

    form.username.data = user_detail.username
    form.documents.choices = document_choices
    if form.validate_on_submit():
        id_user = user_detail.id
        documents = form.documents.data

        # delete the old user permission document
        old_user_permission_documents = db.session.query(UserPermissionDocument).filter(id_user == id_user).all()
        for old_data in old_user_permission_documents:
            db.session.delete(old_data)
        
        for document in documents:
            user_permission_document = UserPermissionDocument(
                id_user=id_user,
                id_document=document
            )
            db.session.add(user_permission_document)
        db.session.commit()
        return redirect(url_for('authentication_blueprint.index_permission_user_document'))
    return render_template('permission/document/edit.html', form=form)


@bp.route('/permission/user/document/delete/<int:id_user>', methods=['POST'])
@login_required
def delete_permission_user_document(id_user):
    logger = default_logger(__name__)
    user_permission_documents = db.session.query(UserPermissionDocument).filter(UserPermissionDocument.id_user == id_user).all()
    for user_permission_document in user_permission_documents:
        db.session.delete(user_permission_document)
    db.session.commit()
    return redirect(url_for('authentication_blueprint.index_permission_user_document'))


@bp.route('/user')
@login_required
def index_user():
    logger = default_logger(__name__)
    page = request.args.get('page', 1, type=int)
    users = db.session.query(User).paginate(page, current_app.config['POST_PER_PAGE'], False)

    next_url = url_for('document_blueprint.index_document', page=users.next_num) if users.has_next else None
    prev_url = url_for('document_blueprint.index_document', page=users.prev_num) if users.has_prev else None

    return render_template('user/index.html', users=users.items, next_url=next_url, prev_url=prev_url)


@bp.route('/user/create', methods=['GET', 'POST'])
@login_required
def create_user():
    logger = default_logger(__name__)
    form = UserForm()
    if form.validate_on_submit():
        username = form.username.data
        email = form.email.data
        full_name = form.full_name.data
        role = form.role.data
        password = form.password.data
        if role == 10:
            role = 0

        # check if the username is exist
        user_exist = db.session.query(User).filter(User.username == username).first()
        if user_exist:
            flash('Username already being use, please change')
            return render_template('user/create.html', form=form)

        user = User()
        user.username = username
        user.email = email
        user.full_name = full_name
        user.role = role
        user.set_password(password)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('authentication_blueprint.index_user'))
    return render_template('user/create.html', form=form)


@bp.route('/user/edit/<int:id_user>', methods=['GET', 'POST'])
@login_required
def edit_user(id_user):
    logger = default_logger(__name__)
    user = db.session.query(User).filter(User.id == id_user).first()
    form = UserEditForm(obj=user)
    if form.validate_on_submit():
        username = form.username.data
        email = form.email.data
        full_name = form.full_name.data
        role = form.role.data

        if role == 10:
            role = 0

        user.username = username
        user.email = email
        user.full_name = full_name
        user.role = role

        db.session.commit()
        return redirect(url_for('authentication_blueprint.index_user'))
    return render_template('user/edit.html', form=form)


@bp.route('/user/change_password/<int:id_user>', methods=['GET', 'POST'])
@login_required
def change_password(id_user):
    logger = default_logger(__name__)
    user = db.session.query(User).filter(User.id == id_user).first()
    if not user:
        abort(404)
    form = ChangePasswordForm()
    if form.validate_on_submit():
        old_password = form.old_password.data
        new_password = form.new_password.data
        confirmed_password = form.confirmed_password.data

        if not user.check_password(old_password):
            flash('Current Password is wrong')
            return render_template('user/change_password.html', form=form)
        
        if new_password != confirmed_password:
            flash('New password doesnt match with confirmed password')
            return render_template('user/change_password.html', form=form)
        
        user.set_password(new_password)
        db.session.commit()
        return redirect(url_for('authentication_blueprint.index_user'))
    return render_template('user/change_password.html', form=form)
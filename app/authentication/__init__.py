from flask import Blueprint

authentication_blueprint = Blueprint('authentication_blueprint', __name__, url_prefix='/authentication')

from .views import *
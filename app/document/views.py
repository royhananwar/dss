import datetime
import os
import uuid

from flask import request, render_template, current_app, redirect, url_for, abort
from werkzeug.utils import secure_filename
from flask_login import current_user, login_required
from sqlalchemy import or_, and_

from app import db
from app import document
from app.models.document import Document, Tag, DocumentTag, RequestDocumentPermission
from app.models.authentication import UserPermissionDocument, UserPermissionTag
from app.document import document_blueprint as dp
from app.document.forms import DocumentForm, TagForm, DocumentEditForm, RequestPermissionDocumentForm
from app.utils.pdf_util import convert_pdf_to_text
from app.utils.elasticsearch_util import add_document_to_index
from app.utils.logging_util import default_logger


@dp.route('/')
def index():
    return render_template('layout.html')


@dp.route('/index', methods=['GET'])
@login_required
def index_document():
    logger = default_logger(__name__)
    page = request.args.get('page', 1, type=int)
    if current_user.role == 0:
        documents = db.session.query(Document).paginate(page, current_app.config['POST_PER_PAGE'], False)
    else:
        user_id = current_user.id
        user_permission_documents = db.session.query(UserPermissionDocument.id_document).filter(UserPermissionDocument.id_user == user_id).subquery()
        user_permission_tag = db.session.query(UserPermissionTag.id_tag).filter(UserPermissionTag.id_user == user_id).subquery()
        document_permission_tag = db.session.query(DocumentTag.id_document).filter(DocumentTag.id_tag.in_(user_permission_tag)).subquery()
        documents = db.session.query(Document).filter(or_(
            Document.id.in_(user_permission_documents),
            Document.id.in_(document_permission_tag),
            Document.uploaded_user == user_id)
        ).paginate(page, current_app.config['POST_PER_PAGE'], False)

    next_url = url_for('document_blueprint.index_document', page=documents.next_num) if documents.has_next else None
    prev_url = url_for('document_blueprint.index_document', page=documents.prev_num) if documents.has_prev else None

    return render_template('document/index.html', documents=documents.items, next_url=next_url, prev_url=prev_url)


@dp.route('/create', methods=['GET', 'POST'])
@login_required
def create_document():
    logger = default_logger(__name__)
    form = DocumentForm()
    tag_query = db.session.query(Tag).all()
    choices = [(int(tag.id), tag.name) for tag in tag_query]
    form.tags.choices = choices
    if form.validate_on_submit():
        file_pdf = form.uploaded_file.data
        title_pdf = form.title.data
        original_file_name = secure_filename(file_pdf.filename)
        uploaded_file_name = '{}.pdf'.format(str(uuid.uuid4()))
        tags = form.tags.data
        user_id = current_user.get_id()

        file_pdf.save(
            os.path.join(
                current_app.config['PATH_UPLOAD'], 'pdf', uploaded_file_name
            )
        )
        logger.info("Success upload document {} to {}".format(uploaded_file_name, current_app.config['PATH_UPLOAD']))

        # save to db
        document = Document()
        document.title = title_pdf
        document.original_file_name = original_file_name
        document.uploaded_file_name = uploaded_file_name
        document.body = ''
        document.uploaded_user = user_id
        db.session.add(document)
        db.session.commit()

        for tag in tags:
            dc = DocumentTag(
                id_document=document.id,
                id_tag=tag
            )
            db.session.add(dc)

        db.session.commit()
        return redirect(url_for('document_blueprint.index_document'))
    return render_template('document/create.html', form=form)


@dp.route('/edit/<int:document_id>', methods=['GET', 'POST'])
@login_required
def edit_document(document_id):
    logger = default_logger(__name__)
    document = db.session.query(Document).filter(Document.id == document_id).first()
    document_tags = [i.tag.id for i in document.document_tags]
    form = DocumentEditForm(obj=document)
    tag_query = db.session.query(Tag).all()
    choices = [(int(tag.id), tag.name) for tag in tag_query]
    form.tags.choices = choices
    form.tags.data = document_tags
    form.uploaded_file.data = document.path_file
    if form.validate_on_submit():
        if not form.uploaded_file:
            file_pdf = form.uploaded_file.data
            original_file_name = secure_filename(file_pdf.filename)
            uploaded_file_name = '{}.pdf'.format(str(uuid.uuid4()))
            document.original_file_name = original_file_name
            document.uploaded_file_name = uploaded_file_name
            file_pdf.save(
                os.path.join(
                    current_app.config['PATH_UPLOAD'], 'pdf', uploaded_file_name
                )
            )
            logger.info("Success upload document {} to {}".format(uploaded_file_name, current_app.config['PATH_UPLOAD']))
        title_pdf = form.title.data
        tags = form.tags.data

        document.title = title_pdf
        document.body = ''

        db.session.query(DocumentTag).filter(DocumentTag.id_document == document.id).delete()
        db.session.commit()

        for tag in tags:
            dc = DocumentTag(
                id_document=document.id,
                id_tag=tag
            )
            db.session.add(dc)
        db.session.commit()
        return redirect(url_for('document_blueprint.detail_document', document_id=document.id))
    return render_template('document/edit.html', form=form)


@dp.route('/detail/<int:document_id>', methods=['GET'])
@login_required
def detail_document(document_id):
    document = db.session.query(Document).filter(Document.id == document_id).first()
    return render_template('document/detail.html', document=document)


@dp.route('/permission/index')
@login_required
def index_list_permission():
    logger = default_logger(__name__)
    user_id = current_user.get_id()
    page = request.args.get('page', 1, type=int)
    list_permission = db.session.query(RequestDocumentPermission).filter(RequestDocumentPermission.id_user == user_id).paginate(page, current_app.config['POST_PER_PAGE'], False)
    next_url = url_for('document_blueprint.index_admin_permission_document', page=list_permission.next_num) if list_permission.has_next else None
    prev_url = url_for('document_blueprint.index_admin_permission_document', page=list_permission.prev_num) if list_permission.has_prev else None
    return render_template('/document/index_list_permission.html', permissions=list_permission.items, next_url=next_url, prev_url=prev_url)


@dp.route('/request_permission', methods=['GET', 'POST'])
@login_required
def request_permission_document():
    logger = default_logger(__name__)
    form = RequestPermissionDocumentForm()
    user_id = current_user.get_id()
    user_have_permission = db.session.query(UserPermissionDocument.id_document).filter(UserPermissionDocument.id_user == user_id).subquery()
    user_permission_tag = db.session.query(UserPermissionTag.id_tag).filter(UserPermissionTag.id_user == user_id).subquery()
    document_permission_tag = db.session.query(DocumentTag.id_document).filter(DocumentTag.id_tag.in_(user_permission_tag)).subquery()
    document_query = db.session.query(
        Document
    ).filter(and_(
            ~Document.id.in_(user_have_permission),
            ~Document.id.in_(document_permission_tag),
            Document.uploaded_user != user_id)
    ).all()
    document_choices = [(int(document.id), document.title) for document in document_query]
    form.documents.choices = document_choices
    if form.validate_on_submit():
        user_id = current_user.get_id()
        document_id = form.documents.data
        reason = form.reason.data

        rpd = RequestDocumentPermission(
            id_user = user_id,
            id_document = document_id,
            reason = reason,
            status = 0
        )
        db.session.add(rpd)
        db.session.commit()

        logger.info('Success request the permission')
        return redirect(url_for('document_blueprint.index_document'))
    return render_template('document/request_permission.html', form=form)


@dp.route('/permission/index/admin')
@login_required
def index_admin_permission_document():
    logger = default_logger(__name__)
    page = request.args.get('page', 1, type=int)
    list_permission = db.session.query(RequestDocumentPermission).paginate(page, current_app.config['POST_PER_PAGE'], False)
    next_url = url_for('document_blueprint.index_admin_permission_document', page=list_permission.next_num) if list_permission.has_next else None
    prev_url = url_for('document_blueprint.index_admin_permission_document', page=list_permission.prev_num) if list_permission.has_prev else None
    return render_template('/permission/request/index.html', permissions=list_permission.items, next_url=next_url, prev_url=prev_url)


@dp.route('/permission/response/admin', methods=['POST'])
@login_required
def response_permission():
    logger = default_logger(__name__)
    id_request_permission = int(request.form['id_request_permission'])
    id_status = int(request.form['id_status'])
    id_user = int(request.form['id_user'])
    id_document = int(request.form['id_document'])
    request_permission = db.session.query(RequestDocumentPermission).filter(RequestDocumentPermission.id == id_request_permission).first()
    request_permission.status = id_status
    if id_status == 1:
        upd = UserPermissionDocument(
            id_user = id_user,
            id_document = id_document
        )
        db.session.add(upd)
    request_permission.updated_at = datetime.datetime.now()
    db.session.commit()
    return redirect(url_for('document_blueprint.index_admin_permission_document'))



@dp.route('/tag/index', methods=['GET'])
@login_required
def index_tag():
    logger = default_logger(__name__)

    page = request.args.get('page', 1, type=int)
    tags = db.session.query(Tag).paginate(page, current_app.config['POST_PER_PAGE'], False)
    count_tag = Tag.count()
    logger.info("Total of all tags: {}".format(count_tag))

    # pagination
    next_url = url_for('document_blueprint.index_tag', page=tags.next_num) if tags.has_next else None
    prev_url = url_for('document_blueprint.index_tag', page=tags.prev_num) if tags.has_prev else None

    return render_template('tag/index.html', tags=tags.items, next_url=next_url, prev_url=prev_url)

@dp.route('/tag/create', methods=['GET', 'POST'])
@login_required
def create_tag():
    logger = default_logger(__name__)
    form = TagForm()
    if form.validate_on_submit():
        name = form.name.data

        tag = Tag()
        tag.name = name
        db.session.add(tag)
        db.session.commit()

        logger.info("Tag {} is inserted".format(tag.name))

        return redirect(url_for('document_blueprint.index_tag'))
    
    return render_template('tag/create.html', form=form)

@dp.route('/tag/edit/<int:pk>', methods=['GET', 'POST'])
@login_required
def edit_tag(pk):
    logger = default_logger(__name__)
    tag = db.session.query(Tag).filter(Tag.id==pk).first()
    if not tag:
        abort(404)
    form = TagForm(obj=tag)
    if form.validate_on_submit():
        name = form.name.data

        tag.name = name
        db.session.add(tag)
        db.session.commit()

        logger.info("Tag {} is updated".format(tag.name))
        return redirect(url_for('document_blueprint.index_tag'))
    return render_template('tag/edit.html', form=form)


@dp.route('/tag/delete/<int:pk>', methods=['POST'])
@login_required
def delete_tag(pk):
    logger = default_logger(__name__)
    tag = db.session.query(Tag).filter(Tag.id==pk).first()
    if not tag:
        abort(404)

    db.session.delete(tag)
    db.session.commit()
    
    logger.info("Tag {} is deleted".format(tag.name))

    return redirect(url_for('document_blueprint.index_tag'))


@dp.route('/report/request_permission')
def report_request_permission():
    page = request.args.get('page', 1, type=int)
    list_permission = db.session.query(RequestDocumentPermission).paginate(page, current_app.config['POST_PER_PAGE'], False)
    next_url = url_for('document_blueprint.report_request_permission', page=list_permission.next_num) if list_permission.has_next else None
    prev_url = url_for('document_blueprint.report_request_permission', page=list_permission.prev_num) if list_permission.has_prev else None
    return render_template('/report/request_permission.html', permissions=list_permission.items, next_url=next_url, prev_url=prev_url)
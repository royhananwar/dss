from flask import Blueprint

document_blueprint = Blueprint('document_blueprint', __name__, url_prefix='/document')

from .views import *
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, SelectMultipleField, SelectField, TextAreaField
from wtforms.fields.core import SelectField
from wtforms.validators import DataRequired

from app import db
from app.models.document import Tag


class DocumentForm(FlaskForm):
    title = StringField(validators=[DataRequired()])
    uploaded_file = FileField(validators=[FileRequired()])
    tags = SelectMultipleField(
        'Tags',
        validators=[DataRequired()],
        coerce=int
    )


class DocumentEditForm(FlaskForm):
    title = StringField(validators=[DataRequired()])
    uploaded_file = FileField()
    tags = SelectMultipleField(
        'Tags',
        validators=[DataRequired()],
        coerce=int
    )


class TagForm(FlaskForm):
    name = StringField(validators=[DataRequired()])


class RequestPermissionDocumentForm(FlaskForm):
    documents = SelectField(
        'Document',
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control select2-document',
        },
        coerce=int
    )
    reason = TextAreaField(
        'Reason', 
        validators=[DataRequired()],
        render_kw={
            'class': 'form-control'
        }
    )
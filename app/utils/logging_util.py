import logging
from logging.handlers import RotatingFileHandler

def default_logger(name):

    logger = logging.Logger(name)
    if not logger.hasHandlers():
        formatter = logging.Formatter(
            "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
        handler = RotatingFileHandler('app.log', maxBytes=10000, backupCount=2)
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)

        logger.addHandler(handler)

    return logger
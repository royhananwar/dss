from pdfminer.high_level import extract_text
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument


def convert_pdf_to_text(pdf_file):
    """Convert PDF to Text based on pdfminer.highl_level
    
    Arguments:
        pdf_file {str} -- path file name
    
    Returns:
        document (str) -- extracted pdf text
    """
    text_pdf = extract_text(pdf_file)
    # replace enter with space
    # because it will be inserted in elasticsearch
    text_pdf = text_pdf.replace('\n', '')

    return text_pdf


def get_pdf_object(pdf_file):
    """Get Object PDF based on pdfminer.pdfdocument
    
    Arguments:
        pdf_file {str} -- path file name
    
    Returns:
        document {object} -- object from PDFDocument
    """
    file_open = open(pdf_file, 'rb')
    parser = PDFParser(file_open)
    document = PDFDocument(parser)
    return document
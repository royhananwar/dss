
from elasticsearch import Elasticsearch, RequestsHttpConnection
from flask import current_app

from app.models import db, Document


def get_es():
    """Get elasticsearch connection
    
    Returns:
        es (object) -- Elasticsearch object
    """
    es = Elasticsearch(hosts=current_app.config['ELASTICSEARCH_HOST'])
    return es


def add_to_index(index, _id, payload):
    """Add to index elasticsearch
    
    Arguments:
        index {str} -- Index name
        _id {int} -- id of document
        payload {dict} -- payload to add to index
    """
    es = get_es()
    if not es:
        return
    es.index(index=index, doc_type="_doc", id=id, body=payload)


def remove_from_index(index, id):
    """
    Remove index.
    """
    es = get_es()
    if not es:
        return
    es.delete(index=index, doc_type="_doc", id=id)


def query_index(index, body):
    """
    Query index.
    Return hits (Dict)
    """
    es = get_es()
    if not es:
        return {}

    search = es.search(index=index, doc_type="_doc", body=body)
    return search.get('hits')


# Document Section
def add_document_to_index(document_id):
    """Add document to index
    
    Arguments:
        document_id {int} -- id of the document
    """
    document_info = Document.get_document_info(document_id)

    document_info = {
        'id': document_info.id,
        'title': document_info.title,
        'body': document_info.body,
        'original_file_name': document_info.original_file_name,
        'uploaded_file_name': document_info.uploaded_file_name
    }
    add_to_index(index="dss", _id=document_id, payload=document_info)


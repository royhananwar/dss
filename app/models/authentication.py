import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import Column, Integer, String, DateTime, Text, Table, ForeignKey
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql import func
from flask_login import UserMixin

from app.models.document import Tag, Document
from app import db


class User(UserMixin, db.Model):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(64), index=True, unique=True)
    email = Column(String(120), index=True, unique=True)
    full_name = Column(String(120))
    password_hash = Column(String(128))
    role = Column(Integer, index=True) # 0: admin; 1: normal user
    created_at = Column(DateTime, default=datetime.datetime.utcnow())
    updated_at = Column(DateTime, onupdate=datetime.datetime.utcnow())

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    @property
    def list_tag_permission(self):
        list_user_tag_permission = ','.join(str(i.tag.name) for i in self.user_permission_tag)
        return list_user_tag_permission
    

    @property
    def list_document_permission(self):
        list_user_document_permission = ','.join(str(i.document.title) for i in self.user_permission_document)
        return list_user_document_permission

class UserPermissionTag(db.Model):
    __tablename__ = 'user_permission_tags'
    id = Column(Integer, primary_key=True)
    id_user = Column(Integer, ForeignKey('users.id'))
    id_tag = Column(Integer, ForeignKey('tags.id'))
    
    user = relationship(User, backref=backref("user_permission_tag", cascade="all, delete-orphan"))
    tag = relationship(Tag, backref=backref("user_permission_tag", cascade="all, delete-orphan"))


class UserPermissionDocument(db.Model):
    __tablename__ = 'user_permission_documents'
    id = Column(Integer, primary_key=True)
    id_user = Column(Integer, ForeignKey('users.id'))
    id_document = Column(Integer, ForeignKey('documents.id'))
    
    user = relationship(User, backref=backref("user_permission_document", cascade="all, delete-orphan"))
    document = relationship(Document, backref=backref("user_permission_document", cascade="all, delete-orphan"))

from app import login
@login.user_loader
def load_user(id):
    return User.query.get(int(id))
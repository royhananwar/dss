import datetime

from sqlalchemy import Column, Integer, String, DateTime, Text, Table, ForeignKey
from sqlalchemy.orm import backref, relationship

from flask import current_app

from app import db


class Document(db.Model):

    __tablename__ = 'documents'

    id = Column(Integer, primary_key=True)
    title = Column(String(500))
    original_file_name = Column(String(500))
    uploaded_file_name = Column(String(500))
    body = Column(Text)
    uploaded_user = Column(Integer, ForeignKey('users.id'), nullable=True)
    created_at = Column(DateTime, default=datetime.datetime.utcnow())
    updated_at = Column(DateTime, on_delete=datetime.datetime.utcnow())

    def __str__(self):
        return '<Document {}>'.format(self.name)
    
    @property
    def tag_list(self):
        list_tag_document = ', '.join(str(i.tag.name) for i in self.document_tags)
        return list_tag_document
    
    @staticmethod
    def get_document_info(document_id):
        document_info = (
            db.session.query(Document)
            .filter(Document.id == document_id)
            .first()
        )
        if not document_info:
            return {}
        return document_info
    
    @property
    def path_file(self):
        return '{}/{}'.format(current_app.config['PATH_UPLOAD'], self.uploaded_file_name)


class Tag(db.Model):

    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    created_at = Column(DateTime, default=datetime.datetime.utcnow())

    def __str__(self):
        return '<Tag {}>'.format(self.name)
    
    @staticmethod
    def count():
        return db.session.query(Tag).count()

class DocumentTag(db.Model):

    __tablename__ = 'document_tags'

    id = Column(Integer, primary_key=True)
    id_document = Column(Integer, ForeignKey('documents.id'))
    id_tag = Column(Integer, ForeignKey('tags.id'))
    
    document = relationship(Document, backref=backref("document_tags", cascade="all, delete-orphan"))
    tag = relationship(Tag, backref=backref("document_tags", cascade="all, delete-orphan"))

    def __str__(self):
        return '<DocumentTag {} {}>'.format(self.id_document, self.id_tag)


class RequestDocumentPermission(db.Model):

    __tablename__ = 'request_document_permission'

    id = Column(Integer, primary_key=True)
    id_document = Column(Integer, ForeignKey('documents.id'))
    id_user = Column(Integer, ForeignKey('users.id'))
    status = Column(Integer) # 0 = request; 1 = granted; 3 = denied
    reason = Column(String(500))
    created_at = Column(DateTime, default=datetime.datetime.utcnow())
    updated_at = Column(DateTime, on_update=datetime.datetime.utcnow())

    document = relationship(Document, backref=backref("request_document_permission", cascade="all, delete-orphan"))

    @property
    def user(self):
        from app.models.authentication import User
        return db.session.query(User).filter(User.id == self.id_user).first()
